import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TraderComponent } from './trader/trader.component';
const routes:Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'trader', component: TraderComponent
  },
]
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TraderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
